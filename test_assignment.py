import pytest
import urllib.request

@pytest.fixture
def set_url():
    return "https://the-internet.herokuapp.com/context_menu"


@pytest.mark.parametrize("string_to_find", ["Right-click in the box below to see one called 'the-internet'", "Alibaba"])
def test_found_on_page(set_url, string_to_find):
    html_content = get_html_content(set_url)
    is_found = html_content.find(string_to_find)
    assert is_found != -1


def get_html_content(url):
    html_content = urllib.request.urlopen(url).read()
    return html_content.decode("utf8")
